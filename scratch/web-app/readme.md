This folder contains the working files towards designing a web-based version of the experimental app. 

After a little research, I am using the [Vue.js](https://vuejs.org) framework to build a Single-Page Application. I had also looked at Angular but Vue seems a bit lighter weight and quicker to learn. 

For now I don't think it needs a back-end server or database, but I've looked a bit at node.js to implement if needed. It will need to write data to a file, so that may necessitate. Not sure. 

 
----

To load a .js library in the console, use the following: 

    var script = document.createElement("script");
    script.src = "https://cdn.jsdelivr.net/npm/vue/dist/vue.js"
    document.body.appendChild(script);
