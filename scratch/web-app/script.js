// see accompanying index.html file for more info. 
// examples are from https://vuejs.org/v2/guide/


var app = new Vue({
  el: '#app',
  data: {
    message: 'Hello Vue!'
  }
});

var app2 = new Vue({
	el: '#app-2',
	data: {
		message: 'You loaded this page on ' + new Date().toLocaleString()
	}
});

var app3 = new Vue({
	el: '#app-3',
	data: {
		seen: true
	}
});

var app4 = new Vue({
	el: '#app-4',
	data: {
		todos: [
			{ text: 'thing 1' },
			{ text: 'thing 2' },
			{ text: 'thing 3' },
		]
	}
})

var userInput = new Vue({
	el: '#user-input',
	data: {
		message: 'Hello Vue.js!'
	},
	methods: {
		reverseMessage: function () {
			this.message = this.message.split('').reverse().join('')
		}
	}
})

var twoWayBinding = new Vue({
	el: "#model",
	data: {
		message: "Hello Vue!"
	}
})

Vue.component('todo-item', {
	props: ['todo'],
	template: '<li>{{ todo.text}} </li>'
})

var app7 = new Vue({
	el: '#app-7',
	data: {
		groceryList: [
			{ id: 0, text: 'Vegetables' },
			{ id: 1, text: 'Cheese' },
			{ id: 2, text: 'Bread' },
			{ id: 3, text: 'Chocolate' },
			{ id: 4, text: 'Beer' },
		]
	}
})

for (const file in myFiles) {
	print file.name;
}

for (let i = 0; i < myFiles.length; i++) {
	const thisFile = myFiles[i];
	console.log(thisFile.name)
}