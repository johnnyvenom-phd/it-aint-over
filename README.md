# It ain't over till it's over

**_Theory of Mind, Social Intelligence and Improvising Machines_**

**Question:** How do humans tell when a non-idiomatic improvisation is coming to an end?  Why has it proven very difficult to program a machine to know this?

**Experiment:** Does access to visual information help third parties tell when an improvisation is coming to an end?

**Hypothesis:** It does, which suggests that Theory of Mind is involved in human’s ascertaining that an improvisation is ending.  In particular this may involve codes/clues other than purely auditory ones and/or learning to hear auditory signs the way we learn to read visual signs (like facial expressions). This would be need to be modeled for machines to ascertain the same.

This is a pilot experiment to evaluate when and how subjects perceive when a group musical improvisation is going to end. It is supported by a CIRMMT Agile Seed Funding Award for 2019.

PIs: 

- Eric Lewis
- Ian Gold

Researchers: 

- John Sullivan (gitlab.com/johnnyvenom)
- Ky Grace Brooks 

## First objective: 

- Build a prototype experiment for participants to audition excerpts of non-idiomatic improvisatory music performance and predict when the performance is ending. Participants will be divided into two groups: one will get the clips with audio and video; the other will only get the audio. 

## Primary tasks: 

- Grace: find a dozen video recorded examples to use in a prototype experiment.
- Johnny: Create a proof of concept app to demo the experiment.

----

### Windows standalone options checked: 

Findings: can't build app in windows without the \[standalone\] option "Search for Missing Files" checked. Which means it won't run on a Windows computer that doesn't already have Max installed. Have posted to the Cycling74 forum in search of answers \[[link to post][1]\].

- pass 1 everything ok / app size 330MB
    - [x] CEF Support
    - [x] Cant Close Toplevel Patcher
    - [x] Gen Support
    - [x] Include C74 Resources
    - [x] Make Application Subfolder Searchable
    - [x] Search for Missing Files
- pass 2 everything ok / app size 180 MB
    - [ ] CEF Support
    - [x] Cant Close Toplevel Patcher
    - [x] Gen Support
    - [x] Include C74 Resources
    - [x] Make Application Subfolder Searchable
    - [x] Search for Missing Files
- pass 3 everything ok / app size 168 MB
    - [ ] CEF Support
    - [x] Cant Close Toplevel Patcher
    - [ ] Gen Support
    - [x] Include C74 Resources
    - [ ] Make Application Subfolder Searchable
    - [x] Search for Missing Files
- pass 4 doesnt work / app size 168 MB
    - [ ] CEF Support
    - [x] Cant Close Toplevel Patcher
    - [ ] Gen Support
    - [x] Include C74 Resources
    - [ ] Make Application Subfolder Searchable
    - [ ] Search for Missing Files

[1]: https://cycling74.com/forums/jit-movie~-not-working-in-wondows-standalone/