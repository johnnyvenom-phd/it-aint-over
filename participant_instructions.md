It Ain't Over - pilot study
===========================

**Question:** How do humans tell when a non-idiomatic improvisation is coming to an end?  Why has it proven very difficult to program a machine to know this?

This is a pilot experiment to evaluate when and how subjects perceive when a group musical improvisation is going to end. It is supported by a CIRMMT Agile Seed Funding Award for 2019.

**Co-investigators:** Eric Lewis, Ian Gold  
**Researchers:** John Sullivan, Ky Grace Brooks

----

## Instructions

Anyone is welcome to participate in the experiment. Before you begin you will need to get a unique participant ID code, which I will assign to you.

1. The application "It Ain't Over 2" is located on the Desktop of the leftmost computer at the IDMIL demo desk. If you need to log in to the computer, the correct user is 'IDMIL' and the password is 'vicon'. 
2. Open the application and follow the instructions on the screen. 
3. First you will be asked to locate the folder containing the media for the experiment. Please select `Desktop/it_aint_over_MEDIA`. 
4. You will be asked to fill out the Informed Consent form before beginning; you can skip this step (by pressing 'NEXT' as we are just in the pilot phase. 
5. You will be directed to an online questionnaire before you begin the experiment. Please do fill this out, entering your unique participant ID where indicated. Once finished you may close the browser and return to the app. 
6. In the app, enter your participant ID where requested, and select the `Desktop/it_aint_over_DATA` folder as the location to save your session data. Your data file should be named as your participant ID. ( `my_ID.csv`)
7. The application will guide you through the experiment. 

If you have any questions ask me or send me an [email](mailto:john.sullivan2@mail.mcgill.ca). 

----


